# test_search.py

import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from eazydiner_tester import EazyDinerTester

class Test_search():
    res="Test - Do Not Book"

    def test_search_content(self):
        tester = EazyDinerTester()
        tester.setup()
        tester.start()
        try:
            tester.click_login()
            tester.login("8307284752", "9021")
            tester.open_search_tab()
            assert tester.is_search_content()
        except Exception as e:
            tester.capture_error_screenshot("error_search_content")
            print(e)
        finally:
            tester.teardown()

    def test_search_list(self):
        tester = EazyDinerTester()
        tester.setup()
        tester.start()
        try:
            tester.click_login()
            tester.login("8307284752", "9021")
            tester.open_search_tab()
            tester.search_restaurant(Test_search.res)
            assert tester.is_list_showing()
        except Exception as e:
            tester.capture_error_screenshot("error_search_list")
            print(e)
        finally:
            tester.teardown()
        
    
    
    def test_select_restaurant(self):
        tester = EazyDinerTester()
        tester.setup()
        tester.start()
        try:
            tester.click_login()
            tester.login("8307284752", "9021")
            tester.open_search_tab()
            tester.search_restaurant(Test_search.res)
            tester.select_restaurant(Test_search.res)
            assert tester.is_selected(Test_search.res)
        except Exception as e:
            tester.capture_error_screenshot("error_select_restaurant")
            print(e)
        finally:
            tester.teardown()

    def test_recent_restaurant(self):
        tester = EazyDinerTester()
        tester.setup()
        tester.start()
        try:
            tester.click_login()
            tester.login("8307284752", "9021")
            tester.open_search_tab()
            tester.search_restaurant(Test_search.res)
            tester.select_restaurant(Test_search.res)
            tester.go_to_home_page()
            tester.open_search_tab()
            assert tester.is_recent_showing(Test_search.res)
        except Exception as e:
            tester.capture_error_screenshot("error_recent_restaurant")
            print(e)
        finally:
            tester.teardown()    

    def test_chain_recent(self, chain='Barbeque Nation'):
        tester = EazyDinerTester()
        tester.setup()
        tester.start()
        try:
            tester.click_login()
            tester.login("8307284752", "9021")
            tester.open_search_tab()
            tester.search_restaurant(chain)
            tester.select_restaurant(chain)
            tester.select_chain_res()
            tester.go_to_home_page()
            tester.open_search_tab()
            assert tester.is_recent_showing(chain)
        except Exception as e:
            tester.capture_error_screenshot("error_chain_recent")
            print(e)
        finally:
            tester.teardown()
