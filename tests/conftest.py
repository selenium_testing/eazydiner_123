# import pytest
# import pytest_html
# from selenium import webdriver

# @pytest.fixture(scope='session')
# def browser():
#     driver = webdriver.Chrome()
#     yield driver
#     driver.quit()

# @pytest.hookimpl(tryfirst=True, hookwrapper=True)
# def pytest_runtest_makereport(item, call):
#     print("scrrenshot ")
#     outcome = yield
#     report = outcome.get_result()
#     extras = getattr(report, "extras", [])

#     if "browser" in item.funcargs:
#         driver = item.funcargs["browser"]
#         if driver is not None:
#             if report.when == "call":
#                 # Capture a screenshot if the test failed or was skipped
#                 if report.failed or (report.skipped and hasattr(report, "wasxfail")):
#                     screenshot_path = f"screenshots/{item.name}.png"  
#                     driver.get_screenshot_as_file(screenshot_path)  
#                     extras.append(pytest_html.extras.image(screenshot_path, "Screenshot"))

#                 # Always add URL to the report
#                 extras.append(pytest_html.extras.url("https://new-react-test.easydiner.com/"))

#                 # Add additional HTML only on failure
#                 if report.failed:
#                     extras.append(pytest_html.extras.html("<div>Additional HTML</div>"))

#     report.extras = extras

import os
import pytest
import pytest_html  

# Capture screenshot on failure
@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    if rep.when == "call" and rep.failed:
        tester = item.funcargs.get('tester')
        if tester:
            driver = tester.driver
            screenshot_path = os.path.join("screenshots", f"{item.nodeid.replace('::', '_')}.png")
            if not os.path.exists("screenshots"):
                os.makedirs("screenshots")
            driver.save_screenshot(screenshot_path)
            extra = getattr(rep, 'extra', [])
            extra.append(pytest_html.extras.image(screenshot_path))
            rep.extra = extra

def pytest_html_report_title(report):
    report.title = "EazyDiner Test Report"

def pytest_html_results_summary(prefix, summary, postfix):
    prefix.extend([f"<p>Project: EazyDiner</p>", f"<p>Tester: Harpreet</p>", f"<p>Browser: Chrome</p>"])





