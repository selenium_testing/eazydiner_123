# test_booking.py

import sys
import os
import time

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from eazydiner_tester import EazyDinerTester

class Test_booking():

    res="Test - Do Not Book"
    res1="Mist"
    res2="Cafe Hawkers"
    # def test_date_slot(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     try:
    #         tester.open_search_tab()
    #         tester.search_restaurant(Test_booking.res)
    #         tester.select_restaurant(Test_booking.res)
    #         assert tester.is_slot_date_correct()
    #     except Exception as e:
    #         tester.capture_error_screenshot("error_date_slot")
    #         print(e)
    #     finally:
    #         tester.teardown()

    # def test_time_slot(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     try:
    #         tester.open_search_tab()
    #         tester.search_restaurant(Test_booking.res)
    #         tester.select_restaurant(Test_booking.res)
    #         assert tester.is_slot_time_correct()
    #     except Exception as e:
    #         tester.capture_error_screenshot("error_time_slot")
    #         print(e)
    #     finally:
    #         tester.teardown()

    # def test_redirection_juspay(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     try:
    #         tester.click_login()
    #         tester.login("7717290566", "9021")
    #         tester.open_search_tab()
    #         tester.search_restaurant(Test_booking.res)
    #         tester.select_restaurant(Test_booking.res)
    #         tester.book()
    #         assert tester.is_redirected_juspay()
    #     except Exception as e:
    #         tester.capture_error_screenshot("error_redirection_juspay")
    #         print(e)
    #     finally:
    #         tester.teardown()

    # def test_booking(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     try:
    #         tester.click_login()
    #         tester.login("7717290566", "9021")  
    #         tester.open_search_tab()
    #         tester.search_restaurant(Test_booking.res)
    #         tester.select_restaurant(Test_booking.res)
    #         tester.select_slot("29 Apr")
    #         tester.click_find_best_offer()
    #         tester.free_book()
    #         tester.book()
    #     except Exception as e:
    #         tester.capture_error_screenshot("error_booking")
    #         print(e)
    #     finally:
    #         tester.teardown()

    # def test_active_offer_tab(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     try:
    #         tester.click_login()
    #         tester.login("7717290566", "9021")
    #         tester.open_search_tab()
    #         tester.search_restaurant(Test_booking.res)
    #         tester.select_restaurant(Test_booking.res)
    #         tester.open_deal_offers("Restaurant Offer")
    #         assert tester.is_active_tab_opened("Restaurant Offer")   
    #     except Exception as e:
    #         tester.capture_error_screenshot("error_active_offer_tab")
    #         print(e)
    #     finally:
    #         tester.teardown()

    # def test_location(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     try:
    #         tester.click_login()
    #         tester.login("7717290566", "9021")
    #         tester.location("Hyderabad")
    #     except Exception as e:
    #         tester.capture_error_screenshot("error_location")
    #         print(e)
    #     finally:
    #         tester.teardown()

    # def test_guest_25(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     try:
    #         tester.click_login()
    #         tester.login("7717290566", "9021")
    #         tester.open_search_tab()
    #         tester.search_restaurant(Test_booking.res)
    #         tester.select_restaurant(Test_booking.res)
    #         tester.open_slot_tab()
    #         tester.select_guest("More than 25 Guests")
    #         assert tester.is_guest_popup_visible()
    #         tester.refresh()
    #         tester.open_slot_tab()
    #         tester.select_guest("16-25 Guests")
    #         assert tester.is_guest_popup_visible()
    #     except Exception as e:
    #         tester.capture_error_screenshot("error_guest_25")
    #         print(e)
    #     finally:
    #         tester.teardown()



    def test_calculate_discount(self):
        tester = EazyDinerTester()
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("7717290566", "9021")
        tester.open_search_tab()
        tester.search_restaurant(Test_booking.res1)
        tester.select_restaurant(Test_booking.res1)
        assert tester.is_calculate_discount_correct(1000)    
        tester.teardown()


    # def test_unbookable_restaurant(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()
    #     tester.login("7717290566", "9021")
    #     tester.open_search_tab()
    #     tester.search_restaurant(Test_booking.res2)
    #     tester.select_res_by_location("Ambience Mall, Gurgaon")
    #     tester.book()
    #     assert tester.is_unbookable_error()
    #     tester.teardown()