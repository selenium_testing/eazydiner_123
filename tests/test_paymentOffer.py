import sys
import os
import time

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from eazydiner_tester import EazyDinerTester

class Test_paymentOffer():
    
    # def test_paymentOffer_axis(self):
    #     tester=EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()
    #     tester.login("7717290566","9021")
    #     tester.payment_offers_visible()
    #     tester.click_axis_bank()
    #     tester.click_birthday_offer()
    #     tester.click_link()
    #     assert tester.is_redirected_primePage()==True,"Redirection to prime page failed"
    #     tester.teardown()

    def test_axisBank_offer_presence(self):
        tester=EazyDinerTester()    
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("7717290566","9021")
        tester.payment_offers_visible()
        tester.check_payment_offers()
        time.sleep(2)
        tester.teardown()