import sys
import os
import time


sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from eazydiner_tester import EazyDinerTester

class Test_justpay:

    res="Mist"
    
    def test_dummyCard(self):
        tester=EazyDinerTester()
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("7717290566","9021")
        # tester.book_a_table()
        # tester.click_first_restaurant()
        tester.open_search_tab()
        tester.search_restaurant(Test_justpay.res)
        tester.select_restaurant(Test_justpay.res)
        tester.pay_bill()
        tester.match_available_offers()
        time.sleep(2)
        tester.check_dummy_card()
        tester.teardown()