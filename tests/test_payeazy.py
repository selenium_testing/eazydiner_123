# test_payeazy.py

import sys
import os
import time


sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from eazydiner_tester import EazyDinerTester

class Test_payeazy():

    res="Test - Do Not Book"
    res1="Barbeque Nation" #paybill only after booking 
    res2="Mist" #check net payable
    res3="Mindspace Social" # no payeazy only bookable
    res4="Connaught Clubhouse"

    # def test_payeazy_available(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()  
    #     tester.login("7717290566", "9021")
    #     tester.location("Hyderabad")
    #     tester.open_search_tab()
    #     tester.search_restaurant(Test_payeazy.res3)
    #     tester.select_restaurant(Test_payeazy.res3)
    #     assert not tester.is_redirected_payeazy()
    #     tester.teardown()
        

    # def  test_net_payable(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()
    #     tester.login("8307284752", "9021")
    #     tester.open_search_tab()
    #     tester.search_restaurant(Test_payeazy.res2)
    #     tester.select_restaurant(Test_payeazy.res2)
    #     time.sleep(2)
    #     tester.pay_bill()
    #     assert tester.check_net_payable()


    # def test_juspay_elements(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()
    #     tester.login("8307284752", "9021")
    #     tester.open_search_tab()
    #     tester.search_restaurant(Test_payeazy.res)
    #     tester.select_restaurant(Test_payeazy.res)
    #     tester.book()
    #     assert tester.is_juspay_element_visible()
    #     tester.teardown()

    # def test_check_convienience_fee(self):

    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()
    #     tester.login("8307284752", "9021") 
    #     tester.open_search_tab()
    #     tester.search_restaurant(Test_payeazy.res2)
    #     tester.select_restaurant(Test_payeazy.res2)
    #     time.sleep(2)
    #     tester.pay_bill()
    #     assert tester.check_offers_visible()
        


    # def test_payeazy_text_from_csv(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()
    #     tester.login("8307284752", "9021")
    #     tester.open_search_tab()
    #     tester.search_restaurant(Test_payeazy.res)
    #     tester.select_restaurant(Test_payeazy.res)
    #     tester.pay_bill()
    #     tester.payeazy("1100")
    #     time.sleep(3)

    #     assert tester.check_offers_visible()
    #     tester.teardown()

    # def test_payeazy_redirection(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     try:
    #         tester.click_login()
    #         tester.login("8307284752", "9021")
    #         tester.open_search_tab() 
    #         tester.search_restaurant(Test_payeazy.res)
    #         tester.select_restaurant(Test_payeazy.res)
    #         tester.pay_bill()
    #         assert tester.is_redirected_payeazy()
    #     except Exception as e:
    #         tester.capture_error_screenshot("error_payeazy_redirection")
    #         print(e)
    #     finally:
    #         tester.teardown()

    # def test_payeazy_bbq(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()
    #     tester.login("8307284752", "9021")
    #     tester.open_search_tab()
    #     tester.search_restaurant(Test_payeazy.res1)
    #     tester.select_restaurant(Test_payeazy.res1)
    #     tester.select_chain_res()
    #     tester.pay_bill_bbq()
    #     assert tester.is_redirected_payeazy()==True
    #     tester.payeazy("600")
    #     tester.teardown()
    #     # tester.teardown()


    # def test_count_additional_offers(self):
    #     tester = EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()
    #     tester.login("8307284752", "9021")
    #     tester.open_search_tab()
    #     tester.search_restaurant(Test_payeazy.res)
    #     tester.select_restaurant(Test_payeazy.res)
    #     time.sleep(3)
    #     tester.pay_bill()
    #     assert tester.check_offer_count()
    #     tester.teardown()

    # def test_check_payment_mode(self):
    #     tester=EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()
    #     tester.login("7717290566","9021")
    #     tester.open_search_tab()
    #     tester.search_restaurant(Test_payeazy.res4)
    #     tester.select_restaurant(Test_payeazy.res4)
    #     time.sleep(2)
    #     tester.pay_bill()
    #     assert tester.check_offers_and_payment_mode()
    #     tester.teardown()

    # def test_payeazy_dubai(self):
    #     tester=EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()
    #     tester.login("7717290566","9021")
    #     tester.location("Dubai")
    #     tester.open_search_tab()
    #     tester.search_restaurant("Asha's")
    #     tester.select_restaurant("Asha's")
    #     tester.pay_bill()
    #     assert tester.is_redirected_payeazy()==True,"Redirection failed to payeazy page"
    #     tester.payeazy("200")
    #     tester.click_cross()
    #     tester.click_select_payment_option()
    #     tester.select_payment_way()
    #     time.sleep(2)
    #     assert tester.is_redirected_telr()==True , "Redirection failed to secure telr page"
    #     tester.teardown()