# EazyDiner Automated Testing Framework

Welcome to the EazyDiner Automated Testing Framework! This repository contains automated tests for the EazyDiner application, helping ensure its functionality.

## Overview

This testing framework is designed to automate the testing process for the EazyDiner web application. It utilizes Selenium WebDriver along with Python to automate interactions with the application's UI. The tests cover various functionalities such as user login, restaurant search, booking, payment, and more.

## Features

- Automated testing of EazyDiner web application
- Comprehensive test coverage for key functionalities
- Easily extensible for adding new test cases
- Support for multiple browsers including Chrome, Firefox, and Edge

## Prerequisites

Before running the automated tests, ensure you have the following installed on your system:

- Python 3.x
- Pip (Python package manager)
- Chrome, Firefox, or Edge browser (depending on your choice)


## Setup

1. Clone this repository to your local machine:

```bash
git clone https://gitlab.com/selenium_testing/pom_eazydiner

```

2. Navigate to the project directory:

```bash
cd eazydiner_testing

```

3. Install the required Python packages:

```bash
pip install -r requirements.txt

```


## Usage

To run the automated tests, execute the following command:

```bash
pytest -s tests/

```
Specify the browser in which you want to run the test

- chrome/edge/firefox

