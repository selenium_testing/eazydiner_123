from .base_page import BasePage
from utils.locators import *
import time
from selenium.common.exceptions import TimeoutException

class PaymenOffers_Page(BasePage):

    def click_birthdayOffer_axis(self):
        self.wait_for_clickable(PaymentOffersLocator.BIRTHDAY_OFFER_AXIS)
        self.perform_action(PaymentOffersLocator.BIRTHDAY_OFFER_AXIS)
        print("Clicked on birthday offer.")

    def click_link_birthdayOffer_axis(self):
        self.wait_for_clickable(PaymentOffersLocator.LINK_BOFFER_AXIS)
        self.perform_action(PaymentOffersLocator.LINK_BOFFER_AXIS)
        print("Clicked on the link to activate prime membership")

    def check_prime_page(self,redirected_url):
        if "prime" in redirected_url:
            print("Redirected to prime page")
            return True
        else:
            return False
        
    def check_offers(self):
        img_tags=self.find_elements(OffersLocators.IMG_TAG)
        visible_offers_count = sum(1 for img in img_tags if img.is_displayed())   
        print(f"Number of visible payment offers: {visible_offers_count}")
        for img in img_tags:
            src = img.get_attribute('src').lower()

            if 'axis-bank' in src:
                print("The banner belongs to Axis Bank.")
            elif 'citi-bank' in src:
                print("The banner belongs to Citi Bank.")
            elif 'indusind-bank' in src:
                print("The banner belongs to IndusInd Bank.")
            elif 'hsbc-premeir-mastercard' in src:
                print("The banner belongs to HSBC Premium Mastercard.")
            elif 'vantage-card-bank' in src:
                print("The banner belongs to DBS.")
            elif 'kotak-mahindra-bank' in src:
                print("The banner belongs to kotak Mahindra Bank.")
            else:
                print("Unknown banner.")