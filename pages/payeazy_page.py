from .base_page import BasePage
from .google_sheets import read_google_sheets
from utils.locators import *
from selenium.common.exceptions import TimeoutException
import time
from selenium.webdriver.common.by import By
import pandas as pd
import math
import random

class PayEazyPage(BasePage):

    def check_payeazy_page(self, redirected_url):
        if "payeazy" in redirected_url:
            print("Redirected to payeazy page")
            return True
        else:
            print("Redirection to payeazy page failed")
            return False


    def enter_amount(self, amount):
        self.wait_for_visible(PayEazyLocators.AMOUNT_INPUT)
        self.perform_action(PayEazyLocators.AMOUNT_INPUT, action='input', value=amount)
        print(f"Entered amount: {amount}")

    def click_check_offers(self):
        self.perform_action(PayEazyLocators.CHECK_OFFERS)
        print("Clicked on CHECK OFFERS button")
        # time.sleep(6)
        time.sleep(1)

    def check_error(self):
        try:
            self.wait_for_visible(PayEazyLocators.ERROR_VALUE)
            print("Error Visible")
            time.sleep(1)
            self.perform_action(PayEazyLocators.DISMISS_ERROR)
            print("Clicked on DISMISS button")

        except TimeoutException:
            print("No Error visible")

    
    def get_offer_texts(self):
        elements = self.find_elements(PayEazyLocators.OFFER_ELEMENTS)
        
        offer_texts = [element.text for element in elements]
        # for text in offer_texts:
        #     print("Offer Text:", text)
        return offer_texts
    
    def csv_offers(self):
        df = read_google_sheets()
        offers = df.iloc[:, 4].tolist()
        # for text in offers:
        #     print("Offer Text:", text)
        return offers
        

    def get_offer_count(self):
        offer_count_element = self.find_element(PayEazyLocators.OFFER_COUNT)
        offer_count_text = offer_count_element.text
        offer_count_parts = offer_count_text.split()
        offer_count_number = int(offer_count_parts[0])
        print("Offer count:", offer_count_number)
        return offer_count_number
    
    def count_offers(self):
        elements = self.find_elements(PayEazyLocators.ADDITIONAL_OFFER_NAME)    
        offer_texts = [element.text for element in elements]
        # for text in offer_texts:
        print("Offers visible:", len(offer_texts))
        return len(offer_texts)
    


    def get_convenience_fee(self,value):
        convenience_fee_mapping = {
                (0, 499): 10,
                (500, 999): 10,
                (1000, 1499): 25,
                (1500, 1999): 30,
                (2000, 3499): 45,
                (3500, 4999): 75,
                (5000, 7499): 100,
                (7500, 10000): 100,
                (10001, float('inf')): 100  
            }
        for (lower, upper), fee in convenience_fee_mapping.items():
            if lower <= value <= upper:
                return fee
                    
    # for convienence fee adding after discount
    
    # def check_net_payable(self):
    #     file_path = '../tests/coupons.csv'
    #     df = pd.read_csv(file_path)

    #     flag = True
    #     for index, row in df.iterrows():
    #         coupon_code = row.get('couponCode')
    #         min_txn_amt = int(row.get('minTxnAmt', 0))
    #         discount_type = row.get('discountType', 'percent')
    #         discount = int(row.get('discount', 0))
    #         max_discount = int(row.get('maxDiscount', 0))

    #         if discount_type == 'percent':
    #             discount_value = (discount / 100) * min_txn_amt
    #         else:
    #             discount_value = discount

    #         if discount_value > max_discount:
    #             discount_value = max_discount

    #         amount = min_txn_amt - discount_value
    #         self.wait_for_visible(PayEazyLocators.AMOUNT_INPUT)
    #         self.perform_action(PayEazyLocators.AMOUNT_INPUT, action='input', value=min_txn_amt)
    #         self.perform_action(PayEazyLocators.CHECK_OFFERS)
    #         self.wait_for_visible(PayEazyLocators.OFFER_ELEMENTS)
    #         self.enter_coupon_code(coupon_code)
    #         self.click_apply_button()
    #         self.wait_for_clickable(RestaurantLocators.OK_THANKS)
    #         self.click_ok_button()
    #         c_fee = self.get_convenience_fee(min_txn_amt)
    #         net_payable = amount + c_fee

    #         net_payable_element = self.find_element(PayEazyLocators.NET_PAYABLE)
    #         net_payable_text = net_payable_element.text
    #         displayed_net_payable = int(net_payable_text.split(' ')[1])
    #         print("net payable:", net_payable)
    #         print("displayed net payable:", displayed_net_payable)
    #         if net_payable == displayed_net_payable:
    #             print(f"**************Row {index + 1}: Pass**************")
    #         else:
    #             flag = False
    #             print(f"**************Row {index + 1}: Fail**************")

    #         self.navigate_back()

    #     if flag:
    #         print("All rows passed")
    #         return True
    #     else:
    #         return False


    
    def check_net_payable(self):
        file_path = 'tests/coupons.csv'
        df = pd.read_csv(file_path)

        flag = True
        for index, row in df.iterrows():
            coupon_code = row.get('couponCode')
            min_txn_amt = int(row.get('minTxnAmt', 0))
            discount_type = row.get('discountType', 'percent')
            discount = int(row.get('discount', 0))
            max_discount = int(row.get('maxDiscount', 0))
            c_fee = self.get_convenience_fee(min_txn_amt)
            amount = min_txn_amt + c_fee

            if discount_type == 'percent':
                discount_value = math.floor((discount / 100) * amount)
            else:
                discount_value = discount

            if discount_value > max_discount:
                discount_value = max_discount

            net_payable = amount - discount_value
            # self.wait_for_visible(PayEazyLocators.AMOUNT_INPUT)
            # self.perform_action(PayEazyLocators.AMOUNT_INPUT, action='input', value=min_txn_amt)
            self.enter_amount(min_txn_amt)
            self.click_check_offers()
            self.wait_for_visible(PayEazyLocators.OFFER_ELEMENTS)
            self.perform_action(RestaurantLocators.ENTER_OFFER,action='input', value = coupon_code)
            self.perform_action(RestaurantLocators.APPLY)
            time.sleep(2)
            self.wait_for_clickable(RestaurantLocators.OK_THANKS)
            self.perform_action(RestaurantLocators.OK_THANKS)

            net_payable_element = self.find_element(PayEazyLocators.NET_PAYABLE)
            net_payable_text = net_payable_element.text
            displayed_net_payable = int(net_payable_text.split(' ')[1])
            print(f"-Expected_net_payable : {net_payable} ")
            print(f"Displayed_net_payable : {displayed_net_payable}")
            if net_payable == displayed_net_payable:
                print(f"-------------------- Row {index + 1}: Pass --------------------")
            else:
                flag = False
                print(f"-------------------- Row {index + 1}: Fail --------------------")

            self.navigate_back()

        if flag:
            print("All rows passed")
            return True
        else:
            return False
        

    import random

    def check_convenience_fee(self):
        ranges = {
            (0, 499),
            (500, 999),
            (1000, 1499),
            (1500, 1999),
            (2000, 3499),
            (3500, 4999),
            (5000, 7499),
            (7500, 10000),
            (10001, float('inf'))
        }

        for lower, upper in ranges:
            value = random.randint(lower, upper)
            self.enter_amount(value)
            time.sleep(2)
            print(f"Value testing for : {value}")
            expected_fee = self.get_convenience_fee(value)
            actual_fee_element = self.find_element(PayEazyLocators.CONVINIENCE_FEE)
            actual_fee_text = actual_fee_element.text
            actual_fee = int(actual_fee_text.split('₹')[1]) 
            if expected_fee == actual_fee:
                print(f"Range ({lower}-{upper}): Pass")
            else:
                print(f"Range ({lower}-{upper}): Fail")
                return False  
            self.navigate_back()
        print("All ranges passed")
        return True


    def check_juspay_card(self):
        if self.find_element(PayEazyLocators.CARD_ELEMENT):
            print("Juspay card element found")
            return True
        else:
            print("Juspay card element not found")
            return False
        
    def check_juspay_netbanking(self):
        if self.find_element(PayEazyLocators.NET_BANKING_ELEMENT):
            print("Juspay netbanking element found")
            return True
        else:
            print("Juspay netbanking element not found")
            return False
        
    def check_juspay_wallet(self):
        if self.find_element(PayEazyLocators.WALLETS_ELEMENT):
            print("Juspay wallet element found")
            return True
        else:
            print("Juspay wallet element not found")
            return False
    
    def check_juspay_upi(self):
        if self.find_element(PayEazyLocators.UPI_ELEMENT):
            print("Juspay upi element found")
            return True
        else:
            print("Juspay upi element not found")
            return False
        
    def match_offers(self):
        self.enter_amount(10000)
        self.click_check_offers()
        offers = self.get_offer_texts()
        csv_texts = self.csv_offers()
        actual_set = set(offers)
        print("actual set is: \n",actual_set)
        csv_set = set(csv_texts)
        print("csv set is :\n",csv_set)
        if actual_set == csv_set:
            print( "__Offers Matched__" )
            # return True
        else:
            extra_offers = actual_set.symmetric_difference(csv_set)
            print("Extra offers found:/n")
            for offer in extra_offers:
                if offer in actual_set:
                    print("- Extra offer found in webpage:", offer)
                else:
                    print("- Extra offer found in CSV file:", offer)
            # return False   
        self.perform_action(PayEazyLocators.CLOSE_OFFERS_SECTION)
        
    def check_multiple_offers(self):
        self.match_offers()
        self.navigate_back()

        df=read_google_sheets()
        flag=True
        for index, row in df.iterrows():
            # if index:
            #     continue
            coupon_code = row.get('Offer Code')
            # if coupon_code=='CREDUPI' or 'INDUSIND' in coupon_code or 'AXBDAY' in coupon_code or coupon_code=='' or 'DBSDC500' in coupon_code or 'DBSI750' in coupon_code or 'MASTERCARD' in coupon_code:
            #     continue
            print(f"Testing coupon : {coupon_code}")
            desc=row.get('Offer Details') #for checking type of payment
            min_txn_amt_str = (row.get('MOV', '1000'))
            payment_type=(row.get('Card Type', 'UPI'))
            if min_txn_amt_str: 
                min_txn_amt = int(min_txn_amt_str)
            else:
                min_txn_amt = 1000
                
            #checking amount entered for avoiding error
            if min_txn_amt<50:
                min_txn_amt = 100
            self.enter_amount(min_txn_amt)
            self.click_check_offers()
            max_discount_str = (row.get('Max Disc Limit', '1000000'))
            if max_discount_str:
                max_discount = int(max_discount_str)
            else:
                max_discount = 1000000
            discount_str= (row.get('Disc Percent / Flat', '0'))
            if discount_str:
                discount = int(discount_str)
            else:   
                discount = 0

            print(f"Discount : {discount}")
            
            # checking discount type
            if discount>0:
                discount_type = 'percent'
            else:
                discount_type = 'flat'
                discount=max_discount
            c_fee = self.get_convenience_fee(min_txn_amt)
            amount = min_txn_amt + c_fee

            #giving Rs 1 as net payable for flat discount exceeding the amount
            if discount_type=='flat' and amount<discount:
                discount_value = amount - 1
    
            #normal discount calculating for percent and flat
            elif discount_type == 'percent':
                discount_value = math.floor((discount / 100) * amount)
            else:
                discount_value = discount

            #checking max discount
            if discount_value > max_discount:
                discount_value = max_discount
            print(f"Discount_value : {discount_value}")

            #checking cashback offers
            if 'cashback' in desc.lower():
                print("CASHBACK OFFER")
                discount_value=0
                amount=amount-c_fee

            net_payable = amount - discount_value
            print(f"Amount after conv_fee : {amount}")
            self.wait_for_visible(PayEazyLocators.OFFER_ELEMENTS)
            self.perform_action(RestaurantLocators.ENTER_OFFER,action='input', value = coupon_code)
            self.perform_action(RestaurantLocators.APPLY)
            time.sleep(2)
            self.wait_for_clickable(RestaurantLocators.OK_THANKS)
            self.perform_action(RestaurantLocators.OK_THANKS)

            net_payable_element = self.find_element(PayEazyLocators.NET_PAYABLE)
            net_payable_text = net_payable_element.text
            displayed_net_payable = int(net_payable_text.split(' ')[1])
            print(f"-Expected_net_payable : {net_payable} ")
            print(f"Displayed_net_payable : {displayed_net_payable}")
            if net_payable == displayed_net_payable:
                print(f"************** Row {index + 1} NET PAYABLE: Pass **************")
            else:
                flag = False
                print(f"************** Row {index + 1} NET PAYABLE: Fail ************************")
            self.perform_action(PayEazyLocators.SELECT_PAYMENT_OPTION)
            time.sleep(3)
            
            self.wait_for_visible(PayEazyLocators.COUPON_APPLIED)
            if 'juspay' in self.driver.current_url.lower():
                print("Juspay redirected")
            else:
                flag = False
                print("Juspay not redirected")
                
            
            
            if 'card' in payment_type.lower():
                card=self.find_element(PayEazyLocators.JUSPAY_CARD).text
                if card:
                    print(f"************** Row {index + 1} PAYMENT METHOD: Pass **************")
                else:
                    flag = False
                    print(f"************** Row {index + 1} PAYMENT METHOD: Fail ************************")
            elif 'wallet' in payment_type.lower():
                wallet=self.find_element(PayEazyLocators.JUSPAY_WALLETS).text
                if wallet:
                    print(f"************** Row {index + 1} PAYMENT METHOD: Pass **************")
                else:
                    flag = False
                    print(f"************** Row {index + 1} PAYMENT METHOD: Fail ************************")
            else:
                upi=self.find_element(PayEazyLocators.JUSPAY_UPI).text
                if upi:
                    print(f"************** Row {index + 1} PAYMENT METHOD: Pass **************")
                else:
                    flag = False
                    print(f"************** Row {index + 1} PAYMENT METHOD: Fail ************************")
            
            time.sleep(3)
            self.navigate_back()
            self.wait_for_visible(PayEazyLocators.CLOSE_OFFERS_SECTION)
            self.perform_action(PayEazyLocators.CLOSE_OFFERS_SECTION)
            print("Closed offers")
            self.navigate_back()
                
        if flag:
            print("All rows passed")
            return True
        else:
            return False
            
    def click_payment_method(self):
        self.wait_for_clickable(PayEazyLocators.SELECT_PAYMENT_WAY)
        self.perform_action(PayEazyLocators.SELECT_PAYMENT_WAY)
        print("Clicked on payment method")

    def check_telr_page(self,redirected_url):
        if "gateway" in redirected_url:
            print("Redirected to telr page")
            return True
        else:
            return False