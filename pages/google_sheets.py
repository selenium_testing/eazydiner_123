import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas  as pd


def read_google_sheets():
    scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name(r'C:\Users\Harpreet Kaur\Desktop\eazydiner_123\key\tokyo-data-420710-6a5c0f691064.json', scope)

    client = gspread.authorize(creds)
    spreadsheet = client.open_by_url('https://docs.google.com/spreadsheets/d/147J7JK8kySOz6ZJBiF5meD_zAjSs-Q2CLLqxLcP8gD4/edit#gid=1097943396')

    worksheet = spreadsheet.get_worksheet(0)
    data = worksheet.get_all_values()

    df = pd.DataFrame(data[1:], columns=data[0])
    return df
