from .base_page import BasePage
from .google_sheets import read_google_sheets
from utils.locators import *
from selenium.common.exceptions import TimeoutException
import time
from selenium.webdriver.common.by import By
import pandas as pd

class JustPayPage(BasePage):

    def enter_amount(self, amount):
        self.wait_for_visible(PayEazyLocators.AMOUNT_INPUT)
        self.perform_action(PayEazyLocators.AMOUNT_INPUT, action='input', value=amount)
        print(f"Entered amount: {amount}")

    def click_check_offers(self):
        self.perform_action(PayEazyLocators.CHECK_OFFERS)
        print("Clicked on CHECK OFFERS button")
        time.sleep(1)

    def check_dummyCard(self):
        self.navigate_back()
        time.sleep(1)
        df=read_google_sheets()
        flag=True
        for index,row in df.iterrows():
            coupon_code = row.get('Offer Code')
            if not coupon_code or coupon_code in ['INDUSIND1000', 'INDUSINDPCNEW500', 'AXBDAY']:
                print("Skipping this iteration as coupon code is empty or one of the specified values")
                continue
            print(f"Testing coupon : {coupon_code}")
            card_type = row.get('Card Type')
            if 'card' not in card_type.lower():
                print("Skipping this iteration as 'Card Type' is not 'credit card'")
                continue
            desc=row.get('Offer Details') #for checking type of payment
            min_txn_amt_str = (row.get('MOV', '1000'))
            # payment_type=card_type
            if min_txn_amt_str and min_txn_amt_str!= '1' and min_txn_amt_str!='0': 
                min_txn_amt = int(min_txn_amt_str)
            else:
                min_txn_amt = 1000

            self.enter_amount(min_txn_amt)
            self.click_check_offers()
            self.wait_for_visible(PayEazyLocators.OFFER_ELEMENTS)
            self.perform_action(RestaurantLocators.ENTER_OFFER,action='input', value = coupon_code)
            self.perform_action(RestaurantLocators.APPLY)
            time.sleep(2)
            self.wait_for_clickable(RestaurantLocators.OK_THANKS)
            self.perform_action(RestaurantLocators.OK_THANKS)

            self.perform_action(PayEazyLocators.SELECT_PAYMENT_OPTION)
            time.sleep(6)           
            self.wait_for_visible(PayEazyLocators.COUPON_APPLIED)
            if 'juspay' in self.driver.current_url.lower():
                print("Juspay redirected")
            else:
                flag = False
                print("Juspay not redirected")
            
            time.sleep(3)
            self.navigate_back()
            self.wait_for_visible(PayEazyLocators.CLOSE_OFFERS_SECTION)
            self.perform_action(PayEazyLocators.CLOSE_OFFERS_SECTION)
            print("Closed offers")
            self.navigate_back()

        if flag:
            print("All rows passed")
            return True
        else:
            return False
        
          