from selenium import webdriver
from selenium.webdriver.chrome.options import Options

chrome_options = Options()
# chrome_options.add_argument("--headless")
chrome_options.add_argument("--disable-gpu")  # Applicable for Windows environments
chrome_options.add_argument("--no-sandbox")  # Bypass OS security model, needed for Linux environments

driver = webdriver.Chrome(options=chrome_options)
